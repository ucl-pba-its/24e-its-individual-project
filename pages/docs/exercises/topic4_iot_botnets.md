# Hvordan sikrer man sine IoT enheder mod IoT botnets

Inden for it-sikkerhed og DDoS angreb høre man ofte om botnets.  
Disse botnets nu til dags består ofte af en masse IoT enheder som er på nettet og står i folks hjem.  
På grund af at de kan tilgå nettet vil trussel aktører gerne have dem som en del af deres botnets og til at bruge til f.eks. at sende requests til hjemmesider uden ejerens viden i en del af et stort DDoS angreb.

# Læringsmål

De valgte læringsmål stammer fra de uddannelsens mål for læringsudbytte fra den nationale
studieordning kapitel 1.

## Viden

Den studerende har viden om...

- Gængse it-sikkerhedstrusler
- Sikringsmekanismer som indgår i sikre systemer
- It-sikkerhedsprincipper til design af sikre systemer

## Færdigheder

Den studerende kan ...

- Anvende, vurdere og formidle it-sikkerhedsstandarder ift. forretningsbehov.
- Vælge, begrunde og formidle velegnede it-sikkerhedstiltag ift. givne forretningsmæssige
scenarier.
- Identificere og argumentere for velegnede valg af relevante mekanismer til at imødegå
identificerede it-sikkerhedstrusler.

## Kompetencer

Den studerende kan ...

- Med udgangspunkt i bl.a. gængse it-sikkerhedsstandarder, håndtere udarbejdelse af
målrettede it-sikkerhedspolitikker og -procedurer ift. forretningsbehov.
- Håndtere komplekse situationer indenfor professionen.
# Forståelse, analyse og implementeringer af SecDevOps principper

Dette projekt omhandler SecDevOps for en webapplikation med en ”in-memory database”.  
Webapplikationen deployes ved anvendelse af Microsoft Azure hosting. Microsoft Azure står
dermed for logningen og monitoreringen af hjemmesiden.  
Rammerne for udviklingsprocessen defineres i et GitHub repository. Dette GitHub repository implementerer
teknologier og procedurer, som skal være med til at definere en effektiv og sikker
udviklingspipeline.  
Teknologierne som bliver anvendt til statisk kode analyse er CodeQL og
SonarCloud.  
Derudover køres der også Unit Tests. Denne pipeline defineres i GitHub
Workflows, som vil blive eksekveret, når der bliver pushed til Master-branchen.
Implementeringerne af alle teknologierne, som benyttes, er dokumenteret på GitLab-pagen.

# Læringsmål

## Viden

- Forståelse for SecDevOps som princip
- Viden om Microsoft Azure
- Viden om SAST
- Viden om DAST

## Færdigheder

- Oprette en pipeline i GitHub workflow
- Oprette Unit Tests
- Anvende SAST teknologier
- Anvende DAST teknologier

## Kompetencer

- Håndtere fejl i Workflow
- Håndtere teknologier, som bruges til SecDevOps
- Håndtere udvælgelsen af teknologier
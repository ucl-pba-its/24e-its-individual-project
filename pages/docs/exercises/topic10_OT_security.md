# OT-Sikkerhed

I dette projekt vil jeg undersøge OT-Sikkerhed, og hvordan det skiller sig ud fra det jeg
har lært omkring IT-sikkerhed gennem studiet.  
I projektet vil jeg undersøge forskellige områder indenfor OT-sikkerhed, som sårbarheder og hvilke
standarder/tiltag som man kan gøre brug af for at mitigere forskellige sårbarheder.  
Vi vil også lave et miniprojekt i projektperioden, hvor jeg vil se efter om jeg
kan opsnappe kommunikationen imellem to plc’ere.

# Læringsmål 

## Viden

Den studerende har viden om

- Kendskab til ofte anvendte angrebsmetoder
- It-sikkerhedsprincipper til design af sikre systemer
- It-sikkerhedstiltag og kan reflektere over forretningsbehov i forhold hertil.

## Færdigheder

Den studerende kan

- Sætte forsvarsmetoder op mod de mest anvendte angrebsmetoder
- Anvende, vurdere og formidle it-sikkerhedsstandarder ift. forretningsbehov
- Mestre relevante designprincipper i forbindelse med udvikling af sikre
systemer.

## Kompetencer

Den studerende kan

- Håndtere udviklingsorienterede situationer herunder:
- Sikre systemer vha. relevante krypterings tiltag
- Selvstændigt tilegne sig viden, færdigheder og kompetencer indenfor it-
sikkerhed/ot-sikkerhed
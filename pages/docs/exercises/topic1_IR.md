# Incident Response and Compliance

For this research project the plan was to decide on some of the learning objecives from the education as a whole and
focus on fulfilling the ones that best related to the Topic. The following objecives were determined to be the most
relevant to the topic in relation to the education.

# Learning Objectives

## Knowledge

- Operational considerations for IT security
- Practical considerations in relation to work within IT security
- Relevant security principles for system security
- Organizational understanding from a security perspective
- Standards and organizations in IT security work
- IT policies and practices

## Skills

- Analyze logs for incidents and follow an audit trail
- Identify vulnerabilities that a network may have
- Assess the security of a given sotiware architecture

## Competencies

- Handle analyzation of which security threats currently need to be dealt with in a specific IT system.
- Prepare a report on the vulnerabilities a network might have
- Handle fundamental tasks as a security officer

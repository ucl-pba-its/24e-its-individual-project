# DevSecOps

I mit selvvalgt fordybelsesområde, vil jeg kigge på DevSecOps, som er en videreudvikling af DevOps, hvor sikkerhedsaspekter integeres ind i alle udviklings og drift processor.  
For at få nået praktisk "hands on", opsættes et test projekt, som består af en rest API med tilhørende database.  
I projektet opsættes CI/CD, som giver mulighed for at opsætte automatisering af statisk kodeanalyse, og andre relevante sikkerheds værktøjer.

Fokus vil være på at få sat et produktions klar platform op, som skal indgå i et CI/CD miljø.

# Læringsmål

## Viden

- It-sikkerhedsprincipper til design af sikre systemer
- Sikringsmekanismer som indgår i sikre systemer

## Færdigheder

- Identificere og argumentere for velegnede valg af relevante mekanismer til at imødegå identificerede it-sikkerhedstrusler
- Mestre relevante designprincipper i forbindelse med udvikling af sikre systemer.

## Kompetencer

- Gennemføre metoder til efterforskning af it-sikkerhedshændelser
- Selvstændigt tilegne sig viden, færdigheder og kompetencer indenfor it-sikkerhed

# Sikkerhed i indlejrede systemer

Der er ikke særlig meget sikkerhed i indlejrede systmer og det kan resultere i sårbarheder som kan udnyttes.
Hvordan kan indlejrede systemer sikres?

# Læringsmål

## Viden

Den studerende har viden om...

- Anvendelse af indlejret systemer
- Managed og unmanaged kode
- Toolchains
- Buffer og buffer overflow
- Firmware
- Crosscompilers


## Færdigheder

Den studerende kan ...

- Programmere med unmanaged code
- Bruge gdb til at analysere en binary
- Bruge kryptografiske signatur til at verificere filer
- Udføre en OS command injection
- Udføre en buffer overflow angreb

## Kompetencer

Den studerende kan ...

- Håndtere sårbar funktioner i unmanaged code
- Håndtere gdb til at analysere en binary


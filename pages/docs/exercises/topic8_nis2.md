# Selvstændig læring om NIS2

I en tid hvor teknologiske fremskridt og digitalisering ændrer vores samfund på utallige måder,
bliver behovet for at forstå og håndtere cybersikkerhed stadig mere og mere vigtigt. Dette fører
til øget opmærksomhed på emnet National Information Security Framework (NIS2), der er
afgørende for at sikre robuste og effektive sikkerhedsforanstaltninger på tværs af
organisationer og lande.
Denne procesrapport fokuserer på min selvstændige læringsproces inden for emnet NIS2.
Formålet med rapporten er at dele mine erfaringer, refleksioner og indsigt opnået i løbet af
projektet, samt at undersøge, hvordan selvstændig læring kan styrke forståelsen og
håndteringen af et emne så bredt som cybersikkerhed.

# Læringsmål

## Læringsmål 1:

- At kunne få et øget læringsudbytte ved selvstændig læring, samt at kunne opstille en realistisk
projektplan.

## Læringsmål 2:

- At opnå en dybere forståelse af NIS2-rammeværket og dets praktiske implikationer for danske
virksomheder, herunder hvordan det påvirker cybersikkerhedspraksis og -krav.

## Læringsmål 3:

- At udvide mine analytiske færdigheder til at kunne evaluere en specifik virksomheds
cybersikkerhedsforanstaltninger i overensstemmelse med NIS2-kravene og identificere
områder til forbedring.
# Compliance med NIS2 for danske virksomheder

Projektets emne er afgrænset til at arbejde med NIS2 direktoratet, skabe en dybere forståelse af hvad
direktivet forsøger at løse, hvad det betyder for danske virksomheder og hvordan virksomhederne
kan opnå compliance i forhold til dette direktiv.

# Læringsmål

## Viden

- Hvad er NIS2
- Hvordan ser ud i Danmark
- Hvad er de konkrete krav der bliver stillet af NIS2
- Hvad er betegnet som sektorer af særlig vigtig betydning
- Hvad er betegnet som sektorer af anden vigtig betydning

## Færdigheder

- At kunne navigere rundt i NIS2's mange krav og tiltag
- At kunne gennem en analytisk tilgang opstille en strategi for en virksomhed til at opnå
compliance med NIS2

## kompetencer

- Forståelse af NIS2 direktivet.
- Modellering med virksomheder og compliance
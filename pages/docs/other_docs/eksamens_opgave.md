---
title: '23E ITS Selvvalgt projekt eksamensbeskrivelse'
subtitle: 'eksamensbeskrivelse'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2022-04-04
email: 'nisi@ucl.dk'
left-header: \today
right-header: eksamensbeskrivelse
skip-toc: false
semester: 23E
hide:
  - footer
---

# Dokumentets indhold

Dette dokument indeholder praktiske informationer samt spørgsmål til eksamen i faget: _xxx_.

# Eksamens beskrivelse

Eksamen er en skriftlig aflevering på wiseflow, den studerendes projektplan samt offentlig tilgængeligt gitlab projekt, der beskriver og dokumenterer projektet, samt den studerendes reflektion over opfyldelse af læringsmålene, udgør eksamensafleveringen.  
Den officielle beskrivelse af eksamen kan findes i [Valgfagskatalog, PBA i IT-Sikkerhed 2024](https://esdhweb.ucl.dk/D24-2490029.pdf) hvilket den studerende bør gøre sig bekendt med.  
 
Eksamen er med intern censur.

Faget er på 5 ECTS point (135 arbejdstimer) og bedømmes efter 7 trins skalaen.

# Eksamens datoer (kun aflevering)

Se [Semesterbeskrivelse 1. og 2. semester - Efterår 2024](https://esdhweb.ucl.dk/D24-2601256.pdf) afsnit 2.3.
Datoer kan i sjældne tilfælde ændre sig, den præcise dato vil fremgå af wiseflow i rimelig tid inden eksamen.

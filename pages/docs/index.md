---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Selvvalgt fordybelsesområde

På dette website finder du materialer til brug i valgfaget *selvvalgt fordybelsesområde*  

Den officielle beskrivelse af faget kan findes i [Valgfagskatalog, PBA i IT-Sikkerhed 2024](https://esdhweb.ucl.dk/D24-2490029.pdf)

Målene med valgfaget selvvalgt fordybelsesområde er at give den studerende mulighed for at vælge og arbejde selvstændigt med et fordybelsesområde indenfor området IT Sikkerhed, samt at den studerende får mulighed for at træne selvstændigt projekt opstilling og eksekvering. 

Den studerende opsætter selv læringsmål ud fra det valgte emne og indenfor rammerne i uddannelsens mål for læringsudbytte fra den nationale studieordning.  

Den studerende sparrer med underviser under hele forløbet, ved deltagelse i obligatoriske
vejledningsmøder.  

Den studerende opstiller egne læringsmål for projektet ud fra nedenstående skabelon:  

## Viden

Efter fuldførelse af valgfaget har den studerende viden om:

- Egen læringsproces
- Opstilling af læringsmål
- Opstilling af projektmål
- Et selvvalgt emne indenfor it-sikkerhed

## Færdigheder

Efter fuldførelse af valgfaget kan den studerende:

- Opstille egen læringsmål.
- Opstille projektmål
- Vurdere egen opfyldelse af lærings-og projektmål.

## Kompetencer

Efter fuldførelse af valgfaget kan den studerende:

- Selvstændigt tilegne sig ny viden og færdigheder inden for et nyt it-sikkerhedsemne.
- Selvstændigt opstille projekt og læringsmål.


# Eksamen

Eksamen er en skriftlig aflevering, bestående af en procesrapport samt et produkt i form af et offentlig tilgængeligt Gitlab projekt.  

Proces rapporten skal som minimum indeholde følgende afsnit:

- Indledning.
- Beskrivelse af det valgte emne.
- Beskrivelse af projektplan.
- Beskrivelse af tilgang til læringsprocessen omkring det valgte emne.
- Refleksioner over projektet samt egen læringsproces.
- Tydelig reference til Gitlab projekt (produktet).

Herudover skal procesrapporten overholde de gængse krav for skriftlige afleveringer, beskrevet i uddannelsens studieordning.  

Gitlab projekt (produktet) skal som minimum indeholde følgende:  

- Dokumentation for projektet samt beskrivelse af læringsproces.
- Udarbejdede eksempler (f.eks. Infrastrukturopsætninger, kode etc.)

## Bedømmelse

Eksamen er med intern censur.  

Prøven bedømmes efter 7-trinsskalaen ved en intern bedømmelse.  
I henhold til Eksamensbekendtgørelsen foretages der altid en individuel bedømmelse.  

Bedømmelsen er en helhedsvurdering af det afleverede materiale (procesrapport og produkt).  

Prøveform er den samme ved en reeksamen. Det er muligt at indlevere en ny skriftlig
opgave.
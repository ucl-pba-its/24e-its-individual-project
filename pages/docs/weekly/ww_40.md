---
Week: xx
Content: xxx
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge 40 - Arbejdsdag uden underviser

### Praktiske mål

- Dokumenteret (på gitlab) fremgang i projektet

## Agenda til vejledningsmøder

- Hvor langt er du i projektplanen ? (liste med afsluttede opgaver)
- Er der ændringer i projektplanen ? (vis ændret projektplan)
- Hvad har du lært indtil videre ? (i forhold til dine læringsmål)
- Hvad vil du nå inden næste vejledning ? (liste med planlagte opgaver)
- Eventuelt

## Skema

### Onsdag

|  Tid  | Aktivitet                |
| :---: | :----------------------- |
| 12:15 | Vejledning studerende 7  |
| 12:45 | Vejledning studerende 8  |
| 13:15 | Vejledning studerende 9  |
| 14:00 | Vejledning studerende 10 |
| 14:30 | Vejledning studerende 11 |
| 15:00 | Vejledning studerende 12 |

Der er afsat maksimum 25 minutter pr. vejledning

## Kommentarer

- ..

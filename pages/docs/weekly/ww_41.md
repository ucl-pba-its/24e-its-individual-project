---
Week: xx
Content: xxx
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge 41 - Vejledning

### Praktiske mål

- Dokumenteret (på gitlab) fremgang i projektet

## Agenda til vejledningsmøder

- Hvor langt er du i projektplanen ? (liste med afsluttede opgaver)
- Er der ændringer i projektplanen ? (vis ændret projektplan)
- Hvad har du lært indtil videre ? (i forhold til dine læringsmål)
- Hvad vil du nå inden næste vejledning ? (liste med planlagte opgaver)
- Eventuelt

## Skema

### Onsdag

|  Tid  | Aktivitet               |
| :---: | :---------------------- |
| 12:15 | Vejledning studerende 1 |
| 12:45 | Vejledning studerende 2 |
| 13:15 | Vejledning studerende 3 |
| 14:00 | Vejledning studerende 4 |
| 14:30 | Vejledning studerende 5 |
| 15:00 | Vejledning studerende 6 |

Der er afsat maksimum 25 minutter pr. vejledning

## Kommentarer

- ..

---
Week: xx
Content: xxx
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge xx - _Titel_

## Emner

Ugens emner er:

- ..
- ..

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- ..
- ..

### Læringsmål der arbejdes med i faget denne uge

## Afleveringer

- ...

## Skema

### Mandag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 12:15 | Introduktion til dagen |
| 15:30 | Selvstændigt arbejde   |

## Kommentarer

- ..
